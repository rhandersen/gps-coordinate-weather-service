﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WeatherService.Models;
using WeatherService.Models.ServiceResponses;
using WeatherService.Services;

namespace WeatherService.Controllers
{
    [Route("api/[controller]")]
    public class WeatherController : Controller
    {

        // GET api/weather/xxxxx/xxxx
        [HttpGet("{lat}/{lon}")]
        public async Task<IActionResult> Get(string lat, string lon)
        {
            try
            {
                LocationServiceResponse locationResponse = await LocationService.GetLocationAsync(lat, lon);
                if (locationResponse.geonames != null && locationResponse.geonames.Count() > 0)
                {
                    Location location = locationResponse.geonames.First();
                    WeatherServiceResponse weather = await WeatherService.Services.WeatherService.GetCurrentWeatherAsync(location);
                    weather.forecast.populateLatest();
                    return Ok(weather);
                }
                else
                {
                    return NotFound("Location could not be found based on GPS coordinates");
                }
            } catch(Exception e){
                return BadRequest(e.Message);
            }

        }

    }
}
