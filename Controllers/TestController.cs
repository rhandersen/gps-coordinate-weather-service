﻿using Microsoft.AspNetCore.Mvc;

namespace WeatherService.Controllers
{
    [Route("/")]
    public class TestController : Controller
    {
        [HttpGet("")]
        public IActionResult Get()
        {
            return Ok("Test went well I guess");
        }

    }
}
