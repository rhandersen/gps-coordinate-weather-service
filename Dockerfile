FROM microsoft/dotnet:2.0-sdk as build-env
WORKDIR /app

# Copy csproj and restore as distinct layers
COPY *.csproj ./
RUN dotnet restore

# Copy everything else and build
COPY . ./
RUN dotnet publish -c Release -o out

# Build runtime image
FROM microsoft/dotnet:2.0-runtime-jessie
WORKDIR /app
COPY --from=build-env /app/out .

EXPOSE 5000/tcp
ENV ASPNETCORE_URLS https://*:5000

ENTRYPOINT ["dotnet", "WeatherService.dll"]