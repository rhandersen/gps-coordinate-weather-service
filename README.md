# README #

### What is this repository for? ###

A generic .NET core implementation of a service for retrieving weather all over the world based on GPS coordinates

Using GEONAMES and Yr.no to provide this awesome service. 

### How do I get set up? ###

The easiest way is to use Docker:

docker build -t weatherservice .

docker run -d -p 8080:5000 -e "GEONAMES_USER={INSERT USERNAME HERE}" --name weatherservice weatherservice

The service can now be found at http://localhost:5000