﻿using System;
using System.Threading.Tasks;
using WeatherService.Models;
using WeatherService.Models.ServiceResponses;
using RestSharp;
using System.Text;
using System.Xml.Serialization;
using System.IO;
using System.Text.Encodings.Web;

namespace WeatherService.Services
{
    public static class WeatherService
    {
        public static async Task<WeatherServiceResponse> GetCurrentWeatherAsync(Location location)
        {
            TaskCompletionSource<IRestResponse> taskCompletion = new TaskCompletionSource<IRestResponse>();

            var client = new RestClient("http://www.yr.no");

            var request = new RestRequest(buildQueryString(location), Method.GET);

            // async with deserialization
            var asyncHandle = client.ExecuteAsync<WeatherServiceResponse>(request, response =>
            {
                taskCompletion.SetResult(response);
            });

            var restResponse = await taskCompletion.Task;

            if (restResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                XmlSerializer ser = new XmlSerializer(typeof(WeatherServiceResponse), new XmlRootAttribute("weatherdata"));

                using (TextReader reader = new StringReader(restResponse.Content))
                {
                    return (WeatherServiceResponse)ser.Deserialize(reader);
                }
            }
            else
            {
                throw new Exception("Could not find weather for the position");
            }
        }

        private static string buildQueryString(Location location)
        {
            UrlEncoder encoder = UrlEncoder.Default;
            StringBuilder sbuilder = new StringBuilder();
            sbuilder.Append("place").Append("/");
            sbuilder.Append(encoder.Encode(location.countryName)).Append("/");
            if (!String.IsNullOrEmpty(location.adminName1)) sbuilder.Append(encoder.Encode(location.adminName1)).Append("/");
            /*if (!String.IsNullOrEmpty(location.adminName2)) sbuilder.Append(location.adminName2).Append("/");
            if (!String.IsNullOrEmpty(location.adminName3)) sbuilder.Append(location.adminName3).Append("/");
            if (!String.IsNullOrEmpty(location.adminName4)) sbuilder.Append(location.adminName4).Append("/");
            if (!String.IsNullOrEmpty(location.adminName5)) sbuilder.Append(location.adminName5).Append("/");*/
            sbuilder.Append(encoder.Encode(location.name)).Append("/");
            sbuilder.Append("forecast.xml");

            return sbuilder.ToString();
        }
    }
}
