﻿using System;
using System.Threading.Tasks;
using Newtonsoft.Json;
using RestSharp;
using WeatherService.Models;
using WeatherService.Models.ServiceResponses;

namespace WeatherService.Services
{
    public static class LocationService
    {
        public static async Task<LocationServiceResponse> GetLocationAsync(string lat, string lon)
        {

            TaskCompletionSource<IRestResponse> taskCompletion = new TaskCompletionSource<IRestResponse>();

            var client = new RestClient("http://api.geonames.org");
            // client.Authenticator = new HttpBasicAuthenticator(username, password);

            var request = new RestRequest("findNearbyPlaceNameJSON?formatted=true&lat={lat}&lng={lon}&username={username}&style=full", Method.GET);
            request.AddUrlSegment("lat", lat); // replaces matching token in request.Resource
            request.AddUrlSegment("lon", lon); // replaces matching token in request.Resource
            request.AddUrlSegment("username", Environment.GetEnvironmentVariable("GEONAMES_USER"));

            // async with deserialization
            var asyncHandle = client.ExecuteAsync<Location>(request, response =>
            {
                taskCompletion.SetResult(response);
            });

            var restResponse = await taskCompletion.Task;


            return JsonConvert.DeserializeObject<LocationServiceResponse>(restResponse.Content);
        }
    }
}
