﻿using System;
using System.Xml.Serialization;

namespace WeatherService.Models.YrForecast
{
    public class Precipitation
    {
        [XmlAttribute("value")]
        public float value;

        [XmlAttribute("minvalue")]
        public float minValue;

        [XmlAttribute("maxvalue")]
        public float maxValue;
    }
}