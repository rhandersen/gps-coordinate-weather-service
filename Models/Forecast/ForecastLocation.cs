﻿using System;
using System.Xml.Serialization;

namespace WeatherService.Models.YrForecast
{
    public class ForecastLocation
    {
        [XmlElement("name")]
        public string name;

        [XmlElement("type")]
        public string type;

        [XmlElement("country")]
        public string country;
    }
}
