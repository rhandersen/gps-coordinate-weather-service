﻿using System;
using System.Xml.Serialization;

namespace WeatherService.Models.YrForecast
{
    public class ForecastTime
    {
        [XmlAttribute("from")]
        public DateTime from { get; set; }

        [XmlAttribute("to")]
        public DateTime to { get; set; }

        [XmlElement("precipitation")]
        public Precipitation precipitation { get; set; }

        [XmlElement("windDirection")]
        public WindDirection windDirection;

        [XmlElement("windSpeed")]
        public WindSpeed windSpeed;

        [XmlElement("temperature")]
        public Temperature temperature;

        [XmlElement("pressure")]
        public Pressure pressure;
    }
}