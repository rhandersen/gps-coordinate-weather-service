﻿using System;
using System.Xml.Serialization;

namespace WeatherService.Models.YrForecast
{
    public class WindDirection
    {
        [XmlAttribute("deg")]
        public float deg;

        [XmlAttribute("code")]
        public string code;

        [XmlAttribute("name")]
        public string name;
    }
}