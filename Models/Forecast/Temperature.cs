﻿using System;
using System.Xml.Serialization;

namespace WeatherService.Models.YrForecast
{
    public class Temperature
    {
        [XmlAttribute("unit")]
        public string unit;

        [XmlAttribute("value")]
        public float value;
    }
}