﻿using System;
using System.Xml.Serialization;

namespace WeatherService.Models.YrForecast
{
    public class WindSpeed
    {
        [XmlAttribute("mps")]
        public float mps;

        [XmlAttribute("name")]
        public string name;
    }
}