﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace WeatherService.Models.YrForecast
{
    public class Forecast
    {
        [XmlArrayItem("time")]
        public ForecastTime[] tabular { get; set; }

        [XmlIgnore]
        public ForecastTime latest { get; set; }

        public void populateLatest(){
            this.latest = tabular[0];
            this.tabular = null;
        }
    }
}