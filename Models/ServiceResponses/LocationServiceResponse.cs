﻿using System.Collections.Generic;

namespace WeatherService.Models.ServiceResponses
{
    public class LocationServiceResponse
    {
        public ICollection<Location> geonames;
    }
}
