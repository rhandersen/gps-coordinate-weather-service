﻿using System.Xml.Serialization;
using WeatherService.Models.YrForecast;

namespace WeatherService.Models.ServiceResponses
{
    [XmlType("weatherdata")]
    public class WeatherServiceResponse
    {
        [XmlElement(ElementName = "location")]
        public ForecastLocation location;

        [XmlElement(ElementName = "forecast")]
        public Forecast forecast;
    }
}
